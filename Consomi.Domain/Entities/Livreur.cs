﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.ComponentModel.DataAnnotations;
//using System.ComponentModel.DataAnnotations.Schema;

namespace Consomi.Domain.Entities
{
    public class Livreur
    {
        public int id { get; set; }
        //[DataType(DataType.EmailAddress)]
        //[Required]
        public string Mail { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        //[Required]
        //[DataType(DataType.Password)]
        //[MinLength(8)]
        public string Password { get; set; }

        public virtual ICollection<Livraison> Livraison { get; set; }
    }
}
