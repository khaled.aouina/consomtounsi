﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consomi.Domain.Entities
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        //Le mot clé virtual permet de charger automatiquement les données liées à la propriété de navigation
        public virtual ICollection<Product> Products { get; set; }
    }
}
