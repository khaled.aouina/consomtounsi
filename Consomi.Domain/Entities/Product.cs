﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consomi.Domain.Entities
{
    public class Product
    {

        public string Image { get; set; }



        [Required(ErrorMessage = "Le champ est obligatoire")]
        [MaxLength(50, ErrorMessage = "La taille maximale dans la base doit être 50")]
        [StringLength(25, ErrorMessage = "La taille maximale saisie doit être 25")]
        public string Nom{ get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Production Date")]
        [DataType(DataType.DateTime)]
        public DateTime DateF { get; set; }

        [Required]
        [Display(Name = "Production Date")]
        [DataType(DataType.DateTime)]
        public DateTime DateE { get; set; }


        [Required(ErrorMessage = "Le champ est obligatoire")]
        [MaxLength(50, ErrorMessage = "La taille maximale dans la base doit être 50")]
        [StringLength(25, ErrorMessage = "La taille maximale saisie doit être 25")]
        public string CAB { get; set; }
        


        [DataType(DataType.Currency)]
        public double Price { get; set; }

        public int ProductId { get; set; }

        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }

        public int? CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category MyCategory { get; set; }
        
    }
}
