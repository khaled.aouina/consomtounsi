﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consomi.Domain.Entities
{
    public class Livraison
    {
        public int id { get; set; }
        public int idLivraison { get; set; }
        public int adresse { get; set; }
        public DateTime Date { get; set; }
        public int heures { get; set; }
    }
}
