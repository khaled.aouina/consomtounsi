﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Consomi.Web.Models
{
    public class ProductVM
    {
        public string Image { get; set; }
        public string Nom { get; set; }
        public string Description { get; set; }
        public DateTime DateF { get; set; }
        public DateTime DateE { get; set; }    
        public string CAB { get; set; }
        public double Price { get; set; }
        public int ProductId { get; set; }      
        public int Quantity { get; set; }
        public int CategoryId { get; set; }
    }
}