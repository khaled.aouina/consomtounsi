﻿using Consomi.Data.Infrastructure;
using Consomi.Domain;
using Consomi.Domain.Entities;
using Consomi.Service;
using Consomi.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Consomi.Web.Controllers
{
    public class ProductController : Controller
    {
        IDataBaseFactory dbf;
        IUnitOfWork uow;
        IService<Product> serviceProd;
        IService<Category> serviceCat;
        public ProductController()
        {
            dbf = new DataBaseFactory();
            uow = new UnitOfWork(dbf);
            serviceProd = new Service<Product>(uow);
            serviceCat = new Service<Category>(uow);
        }
        // GET: Product
        public ActionResult Index()
        {
            //var products = 
            return View(serviceProd.GetAll());
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View(serviceProd.GetById(id));
        }

        // GET: Product/Create
        public ActionResult Create()//int id)
        {
            ViewBag.CategoryId = new SelectList(serviceCat.GetAll(), "CategoryId", "Name");
            return View();

            //if (id > 0)
            //{
            //    ViewBag.Title = "Edit";
            //    return View(serviceProd.GetById(id));
            //}
            //ViewBag.Title = "Create";
            //return View();
        }

        // POST: Product/Create
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Product p, HttpPostedFileBase file)//(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                p.Image = file.FileName;
                serviceProd.Add(p);
                serviceProd.Commit();
                if (file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/"), file.FileName);
                    file.SaveAs(path);
                }
                return RedirectToAction("Index");
            }

            catch (Exception e)
            {
                return Json(e.Message);
            }



        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.CategoryId = new SelectList(serviceCat.GetAll(), "CategoryId", "Name");
            return View();
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection, Product p, HttpPostedFileBase file)
        {
            try
            {
                // TODO: Add insert logic here
                p.Image = file.FileName;
                serviceProd.Add(p);
                serviceProd.Commit();
                if (file.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/"), file.FileName);
                    file.SaveAs(path);
                }
                return RedirectToAction("Index");
            }

            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View(serviceProd.GetById(id));
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                serviceProd.Delete(serviceProd.GetById(id));
                serviceProd.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult GetByCategory(String categorie)
        {
            Category c = new Category();
            foreach (var itme in serviceCat.GetAll().Where(e => e.Name == categorie)) {
                c = itme;
            }
            List<Product> p = new List<Product>();
            foreach (var item in serviceProd.GetAll().Where(e => e.CategoryId == c.CategoryId))
            {
                p.Add(item);
            }
            return View(p);
        }
        public ActionResult stat()
        {




            int A = 0;
            int B = 0;
            int C = 0;

            var MyCategory = serviceCat.GetAll();
            List<Category> cc = new List<Category>();
            foreach (var item in MyCategory)
            {
                cc.Add(item);
            }
            List<Product> p1 = new List<Product>();
            List<Product> p2 = new List<Product>();
            List<Product> p3 = new List<Product>();
            foreach (var itemm in serviceProd.GetAll().Where(e => e.CategoryId == cc[0].CategoryId))
            {
                p1.Add(itemm);
            }
            foreach (var itemm in serviceProd.GetAll().Where(e => e.CategoryId == cc[1].CategoryId))
            {
                p2.Add(itemm);
            }
            foreach (var itemm in serviceProd.GetAll().Where(e => e.CategoryId == cc[2].CategoryId))
            {
                p3.Add(itemm);
            }



            ViewData["countA"] = p1.Count;
            ViewData["countB"] = p2.Count;
            ViewData["countC"] = p3.Count;





            return View();
        }

        [HttpPost]
        public ActionResult Index(string rech)
        {
            var list = serviceProd.GetAll();

            if (!String.IsNullOrEmpty(rech))
            {
                list = list.Where(m => m.Nom.Contains(rech)).ToList();
            }

            return View(list);

        }



    }







}






