﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Consomi.Domain;
namespace Consomi.Web.Controllers
{
    public class SujetController : Controller
    {
        // GET: Sujet
        public ActionResult Index()
        {
            return View();
        }

        // GET: Sujet/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Sujet/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sujet/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Sujet/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Sujet/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Sujet/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Sujet/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
