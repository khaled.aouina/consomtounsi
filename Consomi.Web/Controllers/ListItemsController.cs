﻿using Consomi.Data;
using Consomi.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Consomi.Web.Controllers
{
    [Authorize]
    public class ListItemsController : ApiController

    {
        private ConsomiContext db = new ConsomiContext();
        private static List<ProductVM> productvms { get; set; } = new List<ProductVM>();

        // GET api/<controller>
        public IEnumerable<ProductVM> Get()
        {
            return productvms;
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            var item = productvms.FirstOrDefault(x => x.ProductId == id);
            if (item != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody]ProductVM model)
        {
            if (string.IsNullOrEmpty(model?.Nom))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            var maxId = 0;
            if (productvms.Count > 0)
            {
                maxId = productvms.Max(x => x.ProductId);
            }
            model.ProductId = maxId + 1;
            productvms.Add(model);
            return Request.CreateResponse(HttpStatusCode.Created, model);
        }

        // PUT api/<controller>/5
        public HttpResponseMessage Put(int id, [FromBody]ProductVM model)
        {
            if (string.IsNullOrEmpty(model?.Nom))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            var item = productvms.FirstOrDefault(x => x.ProductId == id);
            if (item != null)
            {
                // Update *all* of the item's properties
                item.Nom = model.Nom;

                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(int id)
        {
            var item = productvms.FirstOrDefault(x => x.ProductId == id);
            if (item != null)
            {
                productvms.Remove(item);
                return Request.CreateResponse(HttpStatusCode.OK, item);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}