﻿using Consomi.Data.Infrastructure;
using Consomi.Domain.Entities;
using Consomi.Service; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consomi.Service
{
    public class ServiceProduct : Service<Product>, IServiceProduct
    {
        public ServiceProduct(IUnitOfWork uow) : base(uow)
        {

        }
        public IEnumerable<Product> GetByCategory(int categoryId)
        {
            return GetMany(p => p.CategoryId == categoryId);
        }
    }
}
