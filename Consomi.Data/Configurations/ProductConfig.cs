﻿using Consomi.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consomi.Data.Configurations
{
    class ProductConfig : EntityTypeConfiguration<Product>
    {
        public ProductConfig()
        {
           
            //One to many 
            //0..1 : HasOptional  
            // 1..1 HasRequired
            HasOptional(p => p.MyCategory)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.CategoryId)
                .WillCascadeOnDelete(false);
            //Type d'héritage Table Par Hiérarchie (TPH)
           
        }

    }
}
