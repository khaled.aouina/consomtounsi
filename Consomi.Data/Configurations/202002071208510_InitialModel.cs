namespace MyFinance.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Biologicals",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        Herbs = c.String(maxLength: 25, unicode: false),
                        DateProd = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Description = c.String(maxLength: 25, unicode: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Price = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        ImageName = c.String(maxLength: 25, unicode: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Providers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Email = c.String(nullable: false, maxLength: 25, unicode: false),
                        IsApproved = c.Boolean(nullable: false),
                        Password = c.String(nullable: false, maxLength: 25, unicode: false),
                        Username = c.String(maxLength: 25, unicode: false),
                        Biological_ProductId = c.Int(),
                        Chemical_ProductId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Biologicals", t => t.Biological_ProductId)
                .ForeignKey("dbo.Chemicals", t => t.Chemical_ProductId)
                .Index(t => t.Biological_ProductId)
                .Index(t => t.Chemical_ProductId);
            
            CreateTable(
                "dbo.Chemicals",
                c => new
                    {
                        ProductId = c.Int(nullable: false),
                        LabName = c.String(maxLength: 25, unicode: false),
                        StreetAddress_StreetAddress = c.String(maxLength: 25, unicode: false),
                        StreetAddress_City = c.String(maxLength: 25, unicode: false),
                        DateProd = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Description = c.String(maxLength: 25, unicode: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Price = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CategoryId = c.Int(nullable: false),
                        ImageName = c.String(maxLength: 25, unicode: false),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Providers", "Chemical_ProductId", "dbo.Chemicals");
            DropForeignKey("dbo.Chemicals", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Providers", "Biological_ProductId", "dbo.Biologicals");
            DropForeignKey("dbo.Biologicals", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Chemicals", new[] { "CategoryId" });
            DropIndex("dbo.Providers", new[] { "Chemical_ProductId" });
            DropIndex("dbo.Providers", new[] { "Biological_ProductId" });
            DropIndex("dbo.Biologicals", new[] { "CategoryId" });
            DropTable("dbo.Chemicals");
            DropTable("dbo.Providers");
            DropTable("dbo.Categories");
            DropTable("dbo.Biologicals");
        }
    }
}
