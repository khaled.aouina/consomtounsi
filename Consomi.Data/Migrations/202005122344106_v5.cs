namespace Consomi.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class v5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.CategoryId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductId = c.Int(nullable: false, identity: true),
                        Image = c.String(),
                        Nom = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        DateF = c.DateTime(nullable: false),
                        DateE = c.DateTime(nullable: false),
                        CAB = c.String(nullable: false, maxLength: 50),
                        Price = c.Double(nullable: false),
                        Quantity = c.Int(nullable: false),
                        CategoryId = c.Int(),
                    })
                .PrimaryKey(t => t.ProductId)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Paniers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idUser = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        idCommande = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Livraisons",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        idLivraison = c.Int(nullable: false),
                        adresse = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        heures = c.Int(nullable: false),
                        Livreur_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Livreurs", t => t.Livreur_id)
                .Index(t => t.Livreur_id);
            
            CreateTable(
                "dbo.Livreurs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Mail = c.String(),
                        Nom = c.String(),
                        Prenom = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Sujets",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        titreSujet = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Livraisons", "Livreur_id", "dbo.Livreurs");
            DropForeignKey("dbo.Products", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Livraisons", new[] { "Livreur_id" });
            DropIndex("dbo.Products", new[] { "CategoryId" });
            DropTable("dbo.Sujets");
            DropTable("dbo.Livreurs");
            DropTable("dbo.Livraisons");
            DropTable("dbo.Paniers");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
        }
    }
}
