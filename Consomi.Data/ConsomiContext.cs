﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Consomi.Domain.Entities;
using System.Data.Entity;

namespace Consomi.Data
{
    public class ConsomiContext : DbContext

    {
        public ConsomiContext() : base("Name = DefaultConnection")
        { }
              public DbSet<Sujet> Sujets { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Commande> Commande { get; set; }
        public DbSet<Livreur> Livreur { get; set; }
        public DbSet<Livraison> Livraison { get; set; }
        public DbSet<Panier> Panier { get; set; }

    }
    }
